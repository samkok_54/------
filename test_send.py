#!/usr/bin/env python
import socket
import time

UDP_IP = "192.168.1.70" #set ip Python
address = ("192.168.1.60", 8080) #set address Arduino
UDP_PORT = 8080 # set port
sock = socket.socket(socket.AF_INET, # Internet
socket.SOCK_DGRAM) # UDP
sock.bind((UDP_IP, UDP_PORT)) #define position
send_data = "BLUE" #define data
while(1):
    print("OK")
    sock.sendto(send_data.encode(), address) #send data
    time.sleep(1)
