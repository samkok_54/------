#include <stdio.h> 
#include <sys/shm.h> // Shared MEM

#include <stdlib.h> //exit
int rd = 0, wr = 0;
int shm_id,shm_s,SHM_SIZE=5;
char *SHDATA ='\0';
int *Size =0;
key_t shm_key;

void* producer()
{
   // printf("Producer Welcome\n");
    int din = 0;
        while(  1) 
		{
			while((*Size)>=SHM_SIZE);//{printf(" Producer full\n" );}; //กรณีที่ (*Size) มีขนาดมากกว่า SHM_SIZEที่เรากำหนดไว้ เพื่อป้องกันการใส่ค่าเกินกว่าที่เรากำหนดไว้

			SHDATA[wr]  = (char)(65+((din++)%26)); //ตัวอักษรภาษาอังกฤษ A-Z
			printf(" Producer<add>: %c\n",SHDATA[wr] ); //แสดงค่าออกทางหน้าจอ		

			wr = (wr+1)%SHM_SIZE; //เพิ่มค่า wr โดยให้ค่าไม่เกิน SHM_SIZE ที่กำหนดไว้ 

			int check =(*Size);
			(*Size)++; //เพิ่มขนาดของ (*Size) เปรียบเสมือนการใส่ค่าเข้าไป
			printf("Producer---> chk= %d , Size=%d ,Size-1=%d\n\n",check,(*Size),(*Size)-1); //แสดงข้อควมเพื่อใช้ในการตรวจสอบสถานะค่าของแต่ละตัวแปร
			//กรณี เช็ค race condition 
			if(check != (*Size)-1){
				printf("\nProducer occur race condition 'sizebefore' %d != '%d-1' %d\n", check,(*Size), (*Size)-1);
				exit(1); 
			}

		}

}
void* consumer()
{  
   // printf("Consumer Welcome\n");
	while(  1) 
		{
			while((*Size)==0);//{printf("consumer wait\n" );};//กรณีที่SHDATA ไม่มีข้อมูลอยู่ จะได้ไม่เกิด error พอถึงบรรทัดต่อไป ที่มีการเรียกใช้ข้อมูลในSHDATA
			printf("-----------------Consumer<removed>:%c\n",SHDATA[rd]);
	
  			
			SHDATA[rd] = '\0'; //หลังจากแสดงข้อมูลเสร็จแล้ว เราก็จะใส่ใหม่ให้ SHDATA[wr] คือ ค่าว่าง
        		rd = (rd + 1) % SHM_SIZE;//เพิ่มค่า wr โดยให้ค่าไม่เกิน SHM_SIZE ที่กำหนดไว้ 
  
			int check = (*Size);      	
			(*Size)--; //ลดขนาดของ (*Size) เปรียบเสมือนการดึงค่าออก
			printf("_______Consumer-->chk= %d , Size=%d ,Size+1=%d\n\n",check,(*Size),(*Size)+1);//แสดงข้อควมเพื่อใช้ในการตรวจสอบสถานะค่าของแต่ละตัวแปร

			//กรณี เช็ค race condition	
			if(check !=(*Size)+1){
				printf("\nConsumer occur race condition 'sizebefore' %d != '%d+1' %d\n", check,(*Size), (*Size)+1);
				exit(1);

			}
			
		}
   
}
int main()
{	shm_id = shmget(shm_key, sizeof(int), IPC_CREAT | 0644); //จองพื้นที่ใน memory 
	SHDATA = (char*)shmat(shm_id, (void*)0, 0); //นำพื้นที่ใน memory มาใช้ สำหรับการเก็บข้อมูลของ producer เพื่อให้สามารถเรียกใช้ได้ใน consumer 
	shm_s = shmget(shm_key, sizeof(int), IPC_CREAT | 0644); //จองพื้นที่ใน memory 
	Size = (int*)shmat(shm_s, (void*)0, 0);  //นำพื้นที่ใน memory มาใช้ สำหรับการเก็บข้อมูลของขนาดของการเก็บค่าของ  SHDATA 

        pid_t con= fork();
	if (con == 0) 
	{
		consumer();//child process
	}
	else if (con> 0) 
	{
                producer();//parent process
	}
	exit(0);
}
