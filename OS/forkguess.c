#include <stdio.h> 
#include <sys/shm.h> // Shared MEM
#include <stdlib.h>//exit
//#include <sys/time.h>
//#include <time.h>

int rd = 0, wr = 0;
int shm_id,shm_PorC,shm_sA;
int *SHDATA = 0;
key_t shm_key;
int minA=1, maxA=100,Ques=0;

int *PorC=0;
int *stateAns=0;

int timeUSec(){
	struct timeval ti;
	gettimeofday(&ti, NULL);
	return (int)(ti.tv_usec*100000ul);
}

int randRange(int min, int max)
        {
            int low=0,hi=0;
	    //เช็คค่า max min ว่าใส่ถูกตำแหน่งหรือไม่ ถ้าไม่จะทำการสลับตำแหน่ง
            if(min<max)
            {
                low=min;
                hi=max+1; // this is done to include max in output.
            }else{
                low=max+1;// this is done to include max in output.
                hi=min;
            }
            srand(timeUSec());
            return (rand()%(hi-low))+low;
        }


void* producer()
{
	Ques = randRange(minA, maxA); //random โจทย์ ตั้งแต่ minA-maxA
	printf("\n\nQuestion is : %d\n", Ques); //แสดงค่า โจทย์
	
	while(1){
		while((*PorC) == 0); //กรณีที่ยังไม่มีการตอบกลับมาของ consumer ให้วนลูปไป

                //นำค่าคำตอบของ consumer มาเช็คกับค่า Ques ที่ถูกกำหนดโดย producer
		if((*SHDATA)== Ques) //กรณีที่ค่าของคำตอบถูกต้อง
		{
			minA = 1;
			maxA = 100;
			//Ques =randRange(minA, maxA); //randomค่าโจทย์ใหม่ ในกรณีที่คำตอบถูกแล้ว
			printf("%d is Correct\n ", (*SHDATA)); //แสดงค่าว่า คำตอบของconsumer ถูกต้อง
			(*stateAns)=1;	//เพื่อบอก consumer ถึงสถานะของคำตอบ
			exit(0);
		}
		else if((*SHDATA) < Ques)//กรณีที่ค่าของคำตอบน้อยกว่าค่าความจริง
		{
  			printf("%d is too less\n ", (*SHDATA));	//แสดงค่าว่า คำตอบของconsumer นั้นน้อยเกินไป		
			(*stateAns)=2; //เพื่อบอก consumer ถึงสถานะของคำตอบ
		}
		else if((*SHDATA)> Ques)//กรณีที่ค่าของคำตอบมากกว่าค่าความจริง
		{
		printf("%d is too much\n", (*SHDATA)); 	//แสดงค่าว่า คำตอบของconsumerนั้นมากเกินไป					 
			(*stateAns)=3; //เพื่อบอก consumer ถึงสถานะของคำตอบ
		}
		(*PorC) = 0; //บอกสถานะของในการทำงานของ consumer/producer 0 คือสถานะของ consumer
	}

}

void* consumer()
{

		while(1)
		{
		while((*PorC) == 1); //กรณีที่producer random เลข ของโจทย์ ให้วนลูปไป

		//เปลี่ยนค่าให้กับ maxA minA โดยดูค่าจาก (*stateAns) ที่ producer กำหนดค่าให้
		if((*stateAns)==1) //กรณีที่ ค่าของ consumer ตรงกับ ค่าที่producerกำหนดไว้
		{	
			minA = 1;
			maxA = 100;	
			exit(0);
		}
		else if((*stateAns)==2)//กรณีที่ ค่าของ consumer น้อยกว่า ค่าที่producerกำหนดไว้
		{		
			minA=(*SHDATA) +1;
		}
		else if((*stateAns)==3) //กรณีที่ ค่าของ consumer มากกว่า ค่าที่producerกำหนดไว้
		{	
			maxA=(*SHDATA)  -1;
		}
	

		// ขั้นตอนของการเดาเลขของ consumer 
		if(minA == maxA)//กรณีที่ค่า minA เท่ากัน maxA ให้ค่า
		{
			(*SHDATA)= minA;	
			//exit(1);
		}
		else
		{
			(*SHDATA)= randRange(minA, maxA); //randomคำตอบใหม่
		}
		printf("\nconsumer : consumer guess %d (%d,%d)\n", (*SHDATA),minA, maxA); //แสดงคำตอบของ consumer และช่วงของmin-max ที่สามารถตอบได้

		//send to producer
		(*PorC)= 1; //บอกสถานะของในการทำงานของ consumer/producer 1 คือสถานะของ producer
	}
}
int main()
{	shm_id = shmget(shm_key, sizeof(int), IPC_CREAT | 0644); //จองพื้นที่ใน memory 
	SHDATA = (int*)shmat(shm_id, (void*)0, 0); //นำพื้นที่ใน memory มาใช้ สำหรับการเก็บข้อมูลที่ consumer 

	shm_sA = shmget(shm_key, sizeof(int), IPC_CREAT | 0644); //จองพื้นที่ใน memory 
	stateAns = (int*)shmat(shm_sA, (void*)0, 0); //นำพื้นที่ใน memory มาใช้ สำหรับการเก็บข้อมูลของstateของคำตอบว่าถูกต้อง น้อยไป หรือมากเกินไป

	shm_PorC = shmget(shm_key, sizeof(int), IPC_CREAT | 0644); //จองพื้นที่ใน memory 
	PorC= (int*)shmat(shm_PorC, (void*)0, 0); //นำพื้นที่ใน memory มาใช้ สำหรับการเก็บข้อมูลของstate การทำงานว่า เป็น producer หรือ consumer

	

        pid_t con= fork();
	if (con == 0) 
	{
		consumer();//child process
	}
	else if (con> 0){
 		producer();//parent process
		}
	else printf("finished,Thanks..!\n");
	exit(0);
}
