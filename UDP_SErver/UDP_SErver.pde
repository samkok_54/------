//Processing Code
// Processing UDP example to send and receive string data from Arduino
// press any key to send the a message
import hypermedia.net.*;
UDP udp; // define the UDP object

void setup() {
  udp = new UDP( this, 5000, "192.168.1.59" ); // create a new datagram connection on port 6000
  udp.log( true ); // <-- printout the connection activity
  udp.listen( true ); // and wait for incoming message
}
void draw() { }


void receive( byte[] data,String ip, int port ) { // <-- default handler
//void receive( byte[] data, String ip, int port ) { // <-- extended handler
  for(int i=0; i < data.length; i++)
  print(char(data[i])); 
  println();
   // String ip = "192.168.1.60"; // the remote IP address
 // int port = 5000; // the destination p
  boolean M_send=udp.send("My Data", ip, port ); // the message to send
  println(M_send);
  
}