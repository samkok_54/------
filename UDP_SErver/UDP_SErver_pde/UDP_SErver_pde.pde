//Processing Code
// Processing UDP example to send and receive string data from Arduino
// press any key to send the a message
import hypermedia.net.*;
UDP udp; // define the UDP object

void setup() {
  udp = new UDP( this, 5000, "192.168.1.59" ); // create a new datagram connection on port 6000
  udp.log( true ); // <-- printout the connection activity
  udp.listen( true ); // and wait for incoming message
}

void receive( byte[] data,String ip, int port ) { // <-- default handler
//void receive( byte[] data, String ip, int port ) { // <-- extended handler
  for(int i=0; i < data.length; i++)
  print(char(data[i])); 
  println();
  
}